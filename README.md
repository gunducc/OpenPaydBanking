# OpenPayd Banking Application

All requested services has been implemented.

For testing purposes, Postman collection can be found in project, PostmanRequests folder.

Used PostgreSQL v12.4 for database and Apache Tomcat v9.0 as container.

A new database with a name 'openpayd' should be created in PostgreSQL. 

Database url, username and password should be changed from persistence.xml, according to the parameters used while installing the database.

All tables and db dependencies will be created automatically after running the application.


