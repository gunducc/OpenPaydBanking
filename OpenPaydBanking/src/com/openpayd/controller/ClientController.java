package com.openpayd.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;

import com.openpayd.entity.Account;
import com.openpayd.entity.Client;
import com.openpayd.entity.Transaction;
import com.openpayd.service.ClientService;
import com.openpayd.service.TransactionService;


@RestController
@RequestMapping("/client")
public class ClientController {
	
	@Autowired
	ClientService clientService;
	
	@Autowired
	TransactionService transactionService;
	
	@RequestMapping(value = "/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
	public Client getClientById(@PathVariable(value = "id") Long clientId) {
		return clientService.getClientById(clientId);		
	}
	
	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<Client> getAllClients(){
		return clientService.getAllClients();
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> saveClient(@Valid @RequestBody Client client) {		
		try {
			if (client.getName().isEmpty())
				throw new Exception("Name can not be empty");
			if (client.getSurname().isEmpty())
				throw new Exception("Surname can not be empty");
		} catch (Exception ex) {
			throw new ResponseStatusException(
			           HttpStatus.BAD_REQUEST, ex.getMessage(), ex);
		}
		Client newClient = clientService.saveClient(client);
		
		return ResponseEntity.ok(newClient);
	}
	
	@PostMapping(value = "/{id}/account",consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createAccountByClient(@PathVariable(value = "id") long clientId, @Valid @RequestBody Account account) {
		try {
		if (!account.getBalanceStatus().equals("DR")&&!account.getBalanceStatus().equals("CR"))	
			throw new Exception("Account balance status can only be DR or CR");
		if (account.getBalance()<0)	
			throw new Exception("Account balance can not be negative");
		if (!account.getType().equals("CURRENT")&&!account.getType().equals("SAVINGS"))
			throw new Exception("Account type can only be CURRENT or SAVINGS");
		} catch(Exception exc) 
		{
			throw new ResponseStatusException(
			           HttpStatus.BAD_REQUEST, exc.getMessage(), exc);
		}
		
		Client client;
		try {
	    client = clientService.getClientById(clientId);
		} catch (Exception ex) {
			throw new ResponseStatusException(
			           HttpStatus.NOT_FOUND, "Client not found", ex);
		}
		Account newAccount = clientService.createAccountByClient(client, account);
		return ResponseEntity.ok(newAccount);
		
		}
	
	
	@GetMapping(value = "/{id}/account/all",produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Account> getClientAccounts(@PathVariable(value = "id") long clientId){
		return clientService.getClientAccounts(clientId);
	}
	
	@GetMapping(value = "/{id}/account/transactions",produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Transaction> getClientTransactions(@PathVariable(value = "id") long clientId){
		return transactionService.getTransactionsByClientId(clientId);
	}
	
	
	
}
