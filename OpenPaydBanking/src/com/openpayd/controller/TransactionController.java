package com.openpayd.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.openpayd.entity.Account;
import com.openpayd.entity.Client;
import com.openpayd.entity.Transaction;
import com.openpayd.entity.TransactionRequest;
import com.openpayd.service.AccountService;
import com.openpayd.service.TransactionService;

@RestController
@RequestMapping("/transfer")
public class TransactionController {
	
	@Autowired
	TransactionService transactionService;
	
	@Autowired
	AccountService accountService;
	
	@RequestMapping(value = "/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
	public Transaction getTransactionById(@PathVariable(value = "id") Long clientId) {
		return transactionService.getTransactionById(clientId);
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> transferAmount(@Valid @RequestBody TransactionRequest transactionRequest) {
		try {
			if (transactionRequest.getCreditAccountId()==0)	
				throw new Exception("Receiver account id is mandatory");
			if (transactionRequest.getDebitAccountId()==0)	
				throw new Exception("Sender account id is mandatory");
			if (transactionRequest.getAmount()<=0)	
				throw new Exception("Amount must be bigger than zero!");
			} catch(Exception exc) 
			{
				throw new ResponseStatusException(
				           HttpStatus.BAD_REQUEST, exc.getMessage(), exc);
			}
		Account accountDr;
		Account accountCr;
		try {
	    accountDr = accountService.getAccountById(transactionRequest.getDebitAccountId());
	    accountCr = accountService.getAccountById(transactionRequest.getCreditAccountId());
		} catch(Exception exc) 
		{
			throw new ResponseStatusException(
			           HttpStatus.NOT_FOUND, "Couldn't find Sender or Receiver account id!", exc);
		}
		Transaction transaction = new Transaction(accountDr, accountCr, transactionRequest.getAmount(), transactionRequest.getMessage());
		Transaction newTransaction = transactionService.transferAmount(transaction);
		return ResponseEntity.ok(newTransaction);
	}
	


}
