package com.openpayd.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.openpayd.entity.Account;
import com.openpayd.entity.Transaction;
import com.openpayd.service.AccountService;
import com.openpayd.service.TransactionService;

@RestController
@RequestMapping("/account")
public class AccountController {
	
	@Autowired
	AccountService accountService;
	
	@Autowired
	TransactionService transactionService;
	
	@GetMapping(value = "/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
	public Account getAccountById(@PathVariable(value = "id") long accountId){
		return accountService.getAccountById(accountId);
	}
	
	@GetMapping(value = "/{id}/transactions",produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Transaction> getAccountTransactions(@PathVariable(value = "id") long accountId){
		return transactionService.getTransactionsByAccountId(accountId);
	}
	
	

}
