package com.openpayd.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.openpayd.entity.Transaction;

@Repository
public interface TransactionRepository extends CrudRepository<Transaction, Long>{

}
