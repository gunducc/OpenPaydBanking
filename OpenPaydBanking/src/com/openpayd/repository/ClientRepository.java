package com.openpayd.repository;

import javax.persistence.Entity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.openpayd.entity.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
	

}
