package com.openpayd.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.openpayd.entity.Account;
import com.openpayd.entity.Address;

@Repository
public interface AccountRepository extends CrudRepository<Account, Long> {
	
	

}
