package com.openpayd.service;

import java.util.List;

import com.openpayd.entity.Account;

public interface AccountService {
	
	public Account getAccountById(long accountId);
	public void saveAccount(Account account);
	public void deleteAccount(long accountId);
	public List<Account> getAccountsByClientId(long clientId);
	public List<Account> getAccountsByTransactionId(long transactionId);

}
