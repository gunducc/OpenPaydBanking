package com.openpayd.service;

import java.util.List;

import com.openpayd.entity.Account;
import com.openpayd.entity.Client;

public interface ClientService {
	
	public Client getClientById(long id);
	public Client saveClient(Client client);
	public void deleteClient(long clientId);
	public List<Client> getAllClients();
	public Account createAccountByClient(Client client, Account account);
	public List<Account> getClientAccounts(long clientId);

}
