package com.openpayd.service;

import java.util.List;

import com.openpayd.entity.Address;

public interface AddressService {
	
	public Address getAddressById(long id);
	public void saveAddress(Address address);
	public void deleteAddress(long addressId);
	public List<Address> getAddressesByClientId(long clientId);
	

}
