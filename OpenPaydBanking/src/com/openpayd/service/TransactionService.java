package com.openpayd.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.openpayd.entity.Account;
import com.openpayd.entity.Transaction;


public interface TransactionService {
	
	public void saveTransaction(Transaction transaction);
	public Transaction getTransactionById(long id); 
	public List<Transaction> getTransactionsByClientId(long clientId);
	public List<Transaction> getTransactionsByAccountId(long accountId);
	public Transaction transferAmount(Transaction transaction);
}
