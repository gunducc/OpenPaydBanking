package com.openpayd.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openpayd.entity.Account;
import com.openpayd.entity.Client;
import com.openpayd.entity.Transaction;
import com.openpayd.repository.AccountRepository;
import com.openpayd.repository.ClientRepository;
import com.openpayd.repository.TransactionRepository;
import com.openpayd.service.TransactionService;

@Service
@Transactional
public class TransactionServiceImpl implements TransactionService {
	
	@Autowired
	TransactionRepository transactionRepository;
	
	@Autowired
	ClientRepository clientRepository;
	
	@Autowired
	AccountRepository accountRepository;

	@Override
	public void saveTransaction(Transaction transaction) {
		transactionRepository.save(transaction);
	}
	
	@Override
	public Transaction transferAmount(Transaction transaction) {
		Account debitAccount =  transaction.getDebitAccount();
		Account creditAccount = transaction.getCreditAccount();
		if (debitAccount.getBalanceStatus().equals("DR")) {
			debitAccount.setBalance(debitAccount.getBalance()+transaction.getAmount());
		} else {
			debitAccount.setBalance(debitAccount.getBalance()-transaction.getAmount());
			if (debitAccount.getBalance()<0) {
				debitAccount.setBalanceStatus("DR");
				debitAccount.setBalance(debitAccount.getBalance()*-1);
			}
		}
		
		if (creditAccount.getBalanceStatus().equals("CR")) {
			creditAccount.setBalance(creditAccount.getBalance()+transaction.getAmount());
		} else {
			creditAccount.setBalance(creditAccount.getBalance()-transaction.getAmount());
			if (creditAccount.getBalance()<0) {
				creditAccount.setBalanceStatus("CR");
				creditAccount.setBalance(creditAccount.getBalance()*-1);
			}
		}
		
		accountRepository.save(debitAccount);
		accountRepository.save(creditAccount);
		saveTransaction(transaction);
		return transaction;
	}

	

	@Override
	public Transaction getTransactionById(long id) {
		return transactionRepository.findById(id).get();
	}

	@Override
	public List<Transaction> getTransactionsByClientId(long clientId) {
		Client client = clientRepository.findById(clientId).get();
		List<Account> accountList =  client.getAccounts();
		List<Transaction> transactions = new ArrayList<Transaction>();
		for (Account account : accountList) {
			transactions.addAll(getTransactionsByAccountId(account.getId()));
		}
		return transactions;
	}

	@Override
	public List<Transaction> getTransactionsByAccountId(long accountId) {
		Account account = accountRepository.findById(accountId).get();
		List<Transaction> transactions = new ArrayList<Transaction>();
		transactions.addAll(account.getCreditTransactions());
		transactions.addAll(account.getDebitTransactions());
		return transactions;
	}



	

}
