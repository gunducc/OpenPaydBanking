package com.openpayd.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openpayd.entity.Account;
import com.openpayd.repository.AccountRepository;
import com.openpayd.service.AccountService;

@Service
@Transactional
public class AccountServiceImpl implements AccountService{
	
	@Autowired
	AccountRepository acctRepo;

	@Override
	public Account getAccountById(long accountId) {
		return acctRepo.findById(accountId).get();
	}

	@Override
	public void saveAccount(Account account) {
		acctRepo.save(account);
	}

	@Override
	public void deleteAccount(long accountId) {
		acctRepo.deleteById(accountId);		
	}

	@Override
	public List<Account> getAccountsByClientId(long clientId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Account> getAccountsByTransactionId(long transactionId) {
		// TODO Auto-generated method stub
		return null;
	}
	


}
