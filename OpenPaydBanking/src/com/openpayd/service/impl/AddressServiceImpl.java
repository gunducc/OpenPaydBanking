package com.openpayd.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openpayd.entity.Address;
import com.openpayd.repository.AddressRepository;
import com.openpayd.service.AddressService;

@Service
@Transactional
public class AddressServiceImpl implements AddressService{
	
	@Autowired
	AddressRepository addRepo;

	@Override
	public Address getAddressById(long id) {
		return addRepo.findById(id).get();
	}

	@Override
	public void saveAddress(Address address) {
		addRepo.save(address);
	}

	@Override
	public void deleteAddress(long addressId) {
		addRepo.deleteById(addressId);		
	}

	@Override
	public List<Address> getAddressesByClientId(long clientId) {
		// TODO Auto-generated method stub
		return null;
	}

}
