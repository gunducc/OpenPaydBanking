package com.openpayd.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openpayd.entity.Account;
import com.openpayd.entity.Client;
import com.openpayd.repository.AccountRepository;
import com.openpayd.repository.ClientRepository;
import com.openpayd.service.ClientService;

@Service
@Transactional
public class ClientServiceImpl implements ClientService {

	@Autowired
	ClientRepository cliRepo;
	
	@Autowired
	AccountRepository accRepo;
	
	@Override
	public Client getClientById(long id) {
		return cliRepo.findById(id).get();
	}

	@Override
	public Client saveClient(Client client) {
		cliRepo.save(client);
		return client;
	}

	@Override
	public void deleteClient(long clientId) {
		cliRepo.deleteById(clientId);
	}

	@Override
	public List<Client> getAllClients() {
		return cliRepo.findAll();
	}

	@Override
	public Account createAccountByClient(Client client, Account account) {
		List<Account> clientAccounts = client.getAccounts();
		clientAccounts.add(account);
		account.setClient(client);
		client.setAccounts(clientAccounts);
		accRepo.save(account);
		return account;
	}

	@Override
	public List<Account> getClientAccounts(long clientId) {
		Client client = cliRepo.findById(clientId).get();
		List<Account> clientAccounts = client.getAccounts();
		return clientAccounts;
	}

}
