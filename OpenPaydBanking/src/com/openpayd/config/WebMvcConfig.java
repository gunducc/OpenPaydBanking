package com.openpayd.config;


	
	import java.util.List;

import org.springframework.context.annotation.Bean;
	import org.springframework.context.annotation.ComponentScan;
	import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
	 
	@Configuration
	@ComponentScan("com.openpayd")
	public class WebMvcConfig extends WebMvcConfigurerAdapter{
	   /* @Bean(name = "viewResolver")
	    public InternalResourceViewResolver getViewResolver() {
	        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
	        viewResolver.setPrefix("/WEB-INF/");
	        viewResolver.setSuffix(".jsp");
	        return viewResolver;
	    }
	    */
	    @Override
	    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {

	        converters.add(new MappingJackson2HttpMessageConverter());
	        super.configureMessageConverters(converters);
	    }
	} 
	


