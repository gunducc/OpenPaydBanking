package com.openpayd.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PersistenceContext;

import org.hibernate.annotations.Cascade;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Client {
	
	@Id
    @GeneratedValue(strategy = GenerationType. IDENTITY)
	private long id;
	private String name;
	private String surname;
	@OneToOne(cascade = CascadeType.ALL)
	private Address primaryAddress;
	@OneToOne(cascade = CascadeType.ALL)
	private Address secondaryAddress;
	@JsonIgnore
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "client")
	private List<Account> accounts;
	
	

	public Client(String name, String surname, Address primaryAddress, Address secondaryAddress) {
		this.name = name;
		this.surname = surname;
		this.primaryAddress = primaryAddress;
		this.secondaryAddress = secondaryAddress;
	}
	
	public Client() {
		
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public Address getPrimaryAddress() {
		return primaryAddress;
	}
	public void setPrimaryAddress(Address primaryAddress) {
		this.primaryAddress = primaryAddress;
	}
	public Address getSecondaryAddress() {
		return secondaryAddress;
	}
	public void setSecondaryAddress(Address secondaryAddress) {
		this.secondaryAddress = secondaryAddress;
	}
	
	public List<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
	}

}
