package com.openpayd.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PersistenceContext;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Account {
	
	@Id
    @GeneratedValue(strategy = GenerationType. IDENTITY)
	private long id;
	private String type;
	private double balance;
	private String balanceStatus;
	private Date dateCreated;
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name="clientId", nullable = false)
	private Client client;
	@JsonIgnore
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "debitAccount")
	private List<Transaction> debitTransactions;
	@JsonIgnore
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "creditAccount")
	private List<Transaction> creditTransactions;
	
	public Account(String type, double balance, String balanceStatus) {
		this.type = type;
		this.balance = balance;
		this.balanceStatus = balanceStatus;
		long millis=System.currentTimeMillis();    
		this.dateCreated = new java.sql.Date(millis);
	}
	
	public Account() {
		
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public String getBalanceStatus() {
		return balanceStatus;
	}
	public void setBalanceStatus(String balanceStatus) {
		this.balanceStatus = balanceStatus;
	}
	public Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public List<Transaction> getDebitTransactions() {
		return debitTransactions;
	}

	public void setDebitTransactions(List<Transaction> debitTransactions) {
		this.debitTransactions = debitTransactions;
	}

	public List<Transaction> getCreditTransactions() {
		return creditTransactions;
	}

	public void setCreditTransactions(List<Transaction> creditTransactions) {
		this.creditTransactions = creditTransactions;
	}

}
