package com.openpayd.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PersistenceContext;

@Entity
public class Transaction {
	
	@Id
    @GeneratedValue(strategy = GenerationType. IDENTITY)
	private long id;
	@ManyToOne
	@JoinColumn(name="debitAccountId", nullable = false)
	private Account debitAccount;
	@ManyToOne
	@JoinColumn(name="creditAccountId", nullable = false)
	private Account creditAccount;
	private double amount;
	private String message;
	private Date dateCreated;
	
	public Transaction(Account debitAccount, Account creditAccount, double amount, String message) {
		super();
		this.debitAccount = debitAccount;
		this.creditAccount = creditAccount;
		this.amount = amount;
		this.message = message;
		this.dateCreated = new Date();

	}
	
	public Transaction() {
		
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Account getDebitAccount() {
		return debitAccount;
	}
	public void setDebitAccount(Account debitAccount) {
		this.debitAccount = debitAccount;
	}
	public Account getCreditAccount() {
		return creditAccount;
	}
	public void setCreditAccount(Account creditAccount) {
		this.creditAccount = creditAccount;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	

}
