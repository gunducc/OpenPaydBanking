package com.openpayd.entity;

public class TransactionRequest {

	private long debitAccountId;
	private long creditAccountId;
	private double amount;
	private String message;
	
	public TransactionRequest(long debitAccountId, long creditAccountId, double amount, String message) {
		super();
		this.debitAccountId = debitAccountId;
		this.creditAccountId = creditAccountId;
		this.amount = amount;
		this.message = message;
	}
	
	public TransactionRequest() {
		
	}
	
	public long getDebitAccountId() {
		return debitAccountId;
	}
	public void setDebitAccountId(long debitAccountId) {
		this.debitAccountId = debitAccountId;
	}
	public long getCreditAccountId() {
		return creditAccountId;
	}
	public void setCreditAccountId(long creditAccountId) {
		this.creditAccountId = creditAccountId;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
	
}
